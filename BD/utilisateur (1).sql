-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  sam. 19 juin 2021 à 12:42
-- Version du serveur :  10.1.26-MariaDB
-- Version de PHP :  7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `inchclass`
--

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `sexe` varchar(255) NOT NULL,
  `date_naiss` date NOT NULL,
  `pays` varchar(255) NOT NULL,
  `telephone` varchar(255) NOT NULL,
  `photo_profile` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mot_pass` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `nom`, `prenom`, `sexe`, `date_naiss`, `pays`, `telephone`, `photo_profile`, `email`, `mot_pass`) VALUES
(50, 'Abouba', 'Sahadatou Nouria', 'Homme', '2021-06-13', 'cameroun', '4436533817', 'Capture dâ€™Ã©cran 2021-05-28 Ã  16.07.49.png', 'fadhilabouba@gmail.com', 'l;nnk,jm'),
(69, 'Abouba', 'Fadhil', 'Autre', '2021-06-30', 'chine', '655669575', 'tÃ©lÃ©chargement (2).jpg', 'fadhilabouba@gmail.com', 'gffth'),
(107, 'Abouba Lamere', 'Fadhil Arif', 'Homme', '2021-06-15', 'cameroun', '655669575', 'moi.jpg', 'fadhilabouba@gmail.com', 'inchwork'),
(123, 'Abouba', 'Fadhil', 'Homme', '2021-06-17', 'canada', '4436533817', 'moi.jpg', 'fadhilabouba@gmail.com', 'p\'uy');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=124;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
