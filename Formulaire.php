<!DOCTYPE html>
<html lang="en">

<head>
  <title>Formulaire d'inscription par ABOUBA</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="Bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="CSS/style.css">


</head>
<body>
  <div class="container-fluid" style="background-image: url(Images/1.jpg);">

    <div class="row">
      <div class="col-md-offset-3 col-md-6" style="background-color:white; border-radius: 15px; margin-top: 50px; margin-bottom: 50px; ">
        <form method="post" action="Traitement.php" enctype="multipart/form-data">

          <h1 style="text-align: center;">CREEZ VOTRE COMPTE</h1>


          <p>
            <label for="nom"> <sub style="color: red;">* </sub>Nom : </label>
            <div class="input-group">
              <input class="form-control" type="text" name="nom" id="Nom" required="" placeholder="Enter Your Name" minlength="4" 
              pattern="^[A-Za-z0-9_.]+${3,20}" title="Veillez saisir un nom">
               <span class="validity input-group-addon" style="background-color: none;"></span>
            </div>
                  <div class="alert alert-danger Erreur"> Veillez Remplir le champ ci dessus </div>
          </p>

          <p>
              <label for="prenom"> <sub style="color: red;">* </sub>Prenom : </label>
              <div class="input-group">
                <input class="form-control" type="text" name="prenom" id="prenom" required="" placeholder="Votre Prenom" minlength="4"
                 pattern="^[A-Za-z0-9_.]+${3,20}">
                <span class="validity input-group-addon" style="background-color: none;"></span>
              </div>
              <div class="alert alert-danger Erreur"> Veillez Remplir le champ ci dessus </div>
            </p>

            <p>
              <label> <sub style="color: red;">* </sub>Sexe : </label>
            </p>

            <p style="margin-left: 40px; font-size: 18px;">
              <label for="Homme"> Homme </label> <input  type="radio" name="sexe" value="Homme" id="Homme"checked="checked" />
              <label for="Femme"> Femme </label> <input  type="radio" name="sexe" value="Femme" id="Femme" />
              <label for="Autre"> Autre </label> <input  type="radio" name="sexe" value="Autre" id="Autre" />
              <div class="alert alert-danger Erreur"> Vous devez faire un choix </div>
            </p>

      
             <p>
              <label for="naissance"> <sub style="color: red;">* </sub>Date de Naissance : </label>
              <div class="input-group">
                <input  class="form-control" type="date" name="date_naiss" id="naissance" required="" placeholder="Exple: 11 - 08 - 2021" pattern="(0[1-9]|[12][0-9]|3[01])[\/](0[1-9]|1[012])[\/](19|20)\d\d">
                <span class="validity input-group-addon" style="background-color: none;"></span>
              </div>
              <div class="alert alert-danger Erreur"> Veillez Remplir le champ ci dessus </div>
            </p>


           <p>
             <label for="telephone"><sub style="color: red;">* </sub> Telephone : </label>
              <div class="input-group">
                <span class=" input-group-addon" style="background-color: white;">
                  <select  style="border: none;">
                    <option value="00247"> +247 </option>
                    <option value="00237" selected="selected" disabled=""> +237 </option>
                    <option value="00241"> +241 </option>
                    <option value="00228"> +228 </option>
                    <option value="00200"> +200 </option>
                    <option value="00225"> +225 </option>
                    <option value="00205"> +1 </option>
                    <option value="00205"> +33 </option>
                    <option value="00205"> +30 </option>
                    <option value="00205"> +285 </option>
                    <option value="00205"> +286 </option>
                    <option value="00205"> +221 </option>
                   </select>
                </span>
                <input id="telephone" class="form-control" type="number" name="telephone" placeholder="Numero de tel" pattern="[0-9]+" minlength="9" maxlength="20">
              </div>
           </p>

            <p>
              <label for="email"> <sub style="color: red;">* </sub>E-mail : </label>
              <div class="input-group">
                <input class="form-control" type="email" name="email" id="email" required="" placeholder="Example@mail.com" pattern="[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([_\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})">
                <span class="validity input-group-addon" style="background-color: none;"></span>
              </div>
              <div class="alert alert-danger Erreur"> Veillez Remplir le champ ci dessus </div> 
            </p>


            <p>
              <label for="password"> <sub style="color: red;">* </sub>Mot de Passe : </label>
              <input class="form-control" type="password" name="mot_pass" id="password" required="" placeholder="Enter a password">
              <div class="alert alert-danger Erreur"> Veillez Remplir le champ ci dessus </div>
            </p>



            <p>
              <label for="pays"><sub style="color: red;">* </sub>Dans quel pays habitez-vous ?</label>

                <select name="pays" id="pays" class="form-control" required="">


                    <optgroup label="Afrique">
                     <option   selected="selected" disabled=""> Choisi ton Pays d'origine </option>
                     <option value="cameroun">Cameroun</option>
                     <option value="tchad">Tchad</option>
                     <option value="congo">Congo</option>
                     <option value="gabon">Gabon</option>
                     <option value="rwanda">Rwanda</option>
                     <option value="RCA">RCA</option>
                     <option value="nigeria">Nigeria</option>
                     <option value="niger">Niger</option>
                     <option value="egypte">Egypte</option>
                    </optgroup>

                    <optgroup label="Amérique">
                     <option value="canada">Canada</option>
                     <option value="etats-unis">Etats-Unis</option>
                     <option value="brazil">Brazil</option>
                     <option value="chili">Chili</option>
                    </optgroup>   

                    <optgroup label="Asie">
                     <option value="chine">Chine</option>
                     <option value="japon">Japon</option>
                    </optgroup>

               </select>
            <div class="alert alert-danger Erreur"> Vous devez faire un choix </div>
          </p>

            <label for="photo"><sub style="color: red;">* </sub>Ajoutez Votre photo (.png, .jpeg, .jpg | 10 Mo ) <br></label>

              <input type="file" name="photo_profile" id="photo" class="form-control" accept="image/*" onchange="loadFile(event)" required="">
              <div class=" col-md-4" style="margin-top: 10px; margin-bottom: 10px; height: 220px; width: 220px; border-radius: 10%;">
               <img id="pp" style="height: 150px; width: 150px; border-radius: 10%;" />
              </div>

            <input type="submit" class="btn btn-block" style="background-color: rgb(0 151 161);margin-bottom: 20px;" value="enregistrer">
             

        </form>
      </div>
    </div>
  </div>

 

  <script type="text/javascript">  // Fonction  désactivation de affichage  « Message  erreur »
        var Error = document.getElementsByClassName('Erreur');
        // var Photo = document.getElementById('Bloc_Photo');
          var Nom = document.getElementById('Nom');

        function desactive() {
          for (var i = 0 ; i < Error.length ; i++) {
              Error[i].style.display = 'none';
          }
          // Photo.style.display = 'none';
        }
        desactive();

      // Nom.onfocus =  function(){
      //  console.log(Nom.value);
      //  if (Nom.value ='') {
      //    Error[0].style.display = 'block';
      //  }else{
      //    console.log(Error[0].value);
      //  }
      // }
  </script>
  <script type="text/javascript"> // previsualisation image dans formulaire
    var loadFile = function(event) {
        var profil = document.getElementById('pp');
        profil.src = URL.createObjectURL(event.target.files[0]);
      };
  </script>
</body>
</html>